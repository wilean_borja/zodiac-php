<?php
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Zodiac Signs</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/styles/style.css">
	<link rel="stylesheet" href="animate.min.css">
</head>
<body class="bg-dark">
	<h1 class="text-center my-5 text-white" id="main-title">Find you Zodiac Sign!</h1>
		
	<div class="col-lg-4 offset-lg-4 text-dark">
		<form class="bg-light p-4" action="controllers/controller.php" method="POST">
			<div class="form-group">
				<label for="name">Enter your Name:</label>
				<input type="text" name="name" class="form-control">
			</div>

			<div class="form-group">
				<label for="birthMonth">Enter your Birth Month:</label>
				<input type="number" name="birthMonth" class="form-control" min="1" max="12">
			</div>

			<div class="form-group">
				<label for="birthDay">Enter your Birth Day:</label>
				<input type="number" name="birthDay" class="form-control" min="1" max="31">
			</div>


			<div class="text-center">
					<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>


</body>
</html>